Sophie-Carole Chobert

M2 Bioinformatique et Biologie des systèmes


# Ralstonia Enrichment

![alt](./figures/communities.png)

## INTRODUCTION

We aim at perform a Gene set enrichment analysis (GSEA) on the organism *Ralstonia Solanacearum* GMI 1000. This strain is of interest for it is an important pathogen causing bacterial wilt on a broad range of plants species throughout a wide geographical distribution. Among its hosts, some are economically important agricultural crops such as tomato, eggplant, banana or potato.

This project is a preparation for an exploration of the entire *R. Solanacearum  GMI 1000* proteome through an enrichment analysis and its comparison with several other species. The enrichment analysis offers an interpretation in term of biological function by enlightening those that are over-represented in a set of genes.

To this respect the proteome will be split into ensembles of genes/proteins which are likely to be involved in a common biological function. In such a case, proteins are often coexpressed, they tend to interact and regarding bacterias, their genes are found in operons.

Biological function characterization is conveyed by the proteins annotations. Here the enrichment analysis is only based on the Gene Ontology (GO) annotation. This annotation facilitates GSEA because its vocabulary is structured and consistent both throughout an individual genome and between genomes.

The present report first exposes how the data is generated and integrated to allow such a study. In a second part it focuses on enrichment measures by presenting them and displaying a comparison study.

## INTEGRATION & DATA PREPARATION

### data

Data to generate the ensembles is collected from stringDB (Version 11), a database which gathers proteomes and evidences of proteins interactions originating from diverse type of sources (text-mining, high-throughput experiments ...).
From this database we collected the following resources:
+ the proteome
+ coexpression between couple of proteins (links probability > 0.7)
+ protein-protein interactions (ppi) data (links probability > 0.7)
+ neighborhood (genomic context) data (links probability > 0.7)

As annotation data we use :
+ the Gene Ontology graph which provides GO terms and the relationships between them
+ Gene Ontology annotation data (goa) for each organisms which gives us by which GO term is annotated a protein


### integration methods

The data integration is done through the graph database platform neo4j. With the help of an R script (R version 4.0.2), the graph database holds as nodes proteins (from stringDB) and GO terms (from the Gene Ontology). As for the edges, they can connect:
+ two proteins nodes where they carry coexpression, ppi or neighborhood data
+ two GOterms nodes where they correspond to either *part of* or *is a* relationship
+ a protein to a GOterm where they translate the annotation connection
+ two proteins nodes from different organisms where they carry their orthology link

This integration is done with the script `loading_script.R`


### sets generation

The R script `building_sets_script.R` queries the neo4j database in order to obtain for each proteomes separately:
+ sets of proteins directly and implicitely (less specific GO terms) annotated by a GO term.
+ files containing genes forming a community in the graph

Extract from `ecoli.go.direct.sets` file
<pre>
GO:0009088	biological_process: threonine biosynthetic process	b0001|b0002|b0003|b0004|b3433|b3940
GO:0006566	biological_process: threonine metabolic process	b0003|b3113|b3772
GO:1901605	biological_process: alpha-amino acid metabolic process	b1439|b3572|b4340
GO:0006520	biological_process: cellular amino acid metabolic process	b0002|b0004|b0186|b0273|b0693|b0870|b0928|b1748|b1761|b1767|b2366|b2440|b2441|b2870|b2957|b2965|b3008|b3117|b3359|b3433|b3708|b3772|b3940|b4054|b4117|b4131|b4245|b4254
</pre>

Regarding the community detection, a combined score is computed integrating coexpression, ppi and neighborhood values held by edges. This score indicating the total weight of edges connecting proteins (nodes) allows graph clustering by the louvain method.

24 communities have been obtained with this method for *Ralstonia solanacearum GMI1000*.


### status of GO annotation

The enrichment outcome depends on the extent of the specie annotation and it's quality. To get an insight of it we will compare *R. solanacearum* with a model organism *E. coli*.

|  specie  | Proteome size | nb GOterm direct  | nb GOterm implicit | nb protein with 1 GOterm |
|:----------:|:----------:|:-------------:|:------:|:------:|
| *E. coli*         |  4127   |  4118  |   5966  | 3840 |
| *R. solanacearum* |  5120   |  1996  |   3362  | 3718 |

***tab1. Some descriptive statistics***

*E.Coli* proteins are annotated by 4118 distinct GOterms and implicitly annotated by 5906 whereas for *R.solanacearum* the results are smaller : 1996 and 3362. However regarding the number of proteins annotated by at least one GOterm, the two organisms are very close although the proteome size is bigger for *R. solanacearum*. Around 72.6% of the proteome of *R. solanacearum* has been annotated by a GOterm.

!["proteins density per number of direct annotations"](./figures/plot_density_annot_direct.png)

***figure1. Proteins density per number of direct annotations***

On the figure 1 we can see by comparison with *E. coli* the depth of proteins annotation of *R. solanacearum*. Most of proteins are annotated with 1-5 GO terms whereas *E. coli*'s annotated proteins hold more often a larger number of GO terms (2 to 15). Although 72.6 % of the proteins are annotated with a GOterm, they seem to be poorly annotated.

<img src="./figures/proportions.png" width="300"> | <img src="./figures/proportions_coli.png" width="300">

***figure2. relative proportion of each ontology. On the left :*** **R. solanacearum** ***and on the right:*** **E. coli**

We explored the 3 ontologies separately. The results shown in the figure 2 answer the question to whether an ontology has been more exploited than the others. It is found almost twice more "molecular function" than "cellular component" occurences. By comparison to *E. coli* the proportions are very similar.

The evolution of the annotation can be heterogeneous within the strain. Then it would be interesting to separately study the ensembles to see the GO annotation state. Since R. Solanacearum has been studied mostly for its pathogenicity. It is likely that the group of genes that it involved in pathogenicity must me more annotated.


## SEARCHING FOR ENRICHED TERMS

For a given set of genes, we want to know which GO terms is associated to it and to what extend. A set enriched in a specific GO term is a set where the GO term is significantly (p-value <0.05) found annotating genes from this list of set.

### about the script

For a given list of genes annotated by GO terms, the occurrence probability of a specific GO term can be modeled by several distributions :

+ hypergeometric
+ binomial (which is a good approximation of hypergeometric for large sets)
+ Chi2

The python script `blastset.py` performs the set comparisons and provides a p-value for each enriched sets by a GO term.

This script provides also a coverage value which indicates the overlapping extent between the two compared sets. It is distributed between 0 and 1, where 0 is a perfect overlap between the two sets.

Each measures are based on the following variables :
+ **the query ensemble size (Q)**
+ **the target ensemble size (T)**
+ **the number of common elements between them (C)**
+ **the population size (G)**

Coverage:

The non statistical coverage value is always proposed, it is a dissimilarity measure and can be of interest in view of the p.value obtained by one of the other methods.

Hypergeometric and binomial measures:

For the hypergeometric measure, we use the repartition function (survival function or the reverse of probability mass function: the sum of probability piled up to a value). The difference between binomial and hypergeometric measures is that the draws made by the first one are with replacement.

Chi2:

This method needs a contingency table  between the query ensemble and the target ensemble :

C | Q-C |  Q  
--- | --- | ---
**T-C**  |  **G-Q-T+C**  | **G-Q**
**T**  |  **G-T**  | **G**

This table has been obtained using the array function provided by the numpy package. The three statistical methods use functions implemented in scipy.stat.    

### example of use

See the README in the `enrichment` section.

## METHODS COMPARISON

### comparison strategies

To compare the different methods, for the 3 statistical methods we will monitore the p-value when changing the values of the previously cited variables (Q, T, C and G).

### execution

We tested several cases where the query size increased up to the population size and we fixed the population size and target size to the following values:

+ G ~ T, both large (5000 / 4900 ~ proteome size)  
+ G ~ T, both small (500 / 490)
+ G = 5000 and T = 500 (corresponds to the largest target in our direct dataset)
+ G = 5000 and T = 50
+ G = 5000 and T = 10
+ G = 5000 and T = 5  (G very large and T small)

The simulations has been done with the script `compare_methods.py` which displays plots for each tested case presented above. We can decide to focus on Q[0,1000] and pval[0,0.2] with the option : "--focus".

example of use:
```{bash}
python compare_methods.py --measure hypergeometric --focus yes
```

### results

large target size | small target size
--- | ---
![alt](./figures/bin_5000_500.png) | ![alt](./figures/bin_5000_5.png)
![alt](./figures/hyp_5000_500.png) | ![alt](./figures/hyp_5000_5.png)
![alt](./figures/chi2_5000_500.png) | ![alt](./figures/chi2_5000_5.png)

***figure3. P-value for each methods with G=5000 and T=500 or T=5***

The hypergeometric shows more tolerance for higher query size in both cases (large and small T size). By symmetry we can affirm that it also shows higher tolerance for high target size. Although binomial and hypergeometric measures are very close and will tend to propose the same enrichment, the hypergeometric measure will propose GOterms where the ratio C/T is smaller for large T.

<div style="page-break-after: always;"></div>

binomial | hypergeometric | chi2
--- | --- | ---
![alt](./figures/bin_5000_10_zoom.png) | ![alt](./figures/hyp_5000_10_zoom.png) |  ![alt](./figures/chi2_5000_10_zoom.png)

***figure4. P-value for each methods with G=5000 and T=10 - focus on Q between 0 and 1000 and p-value up to 0,2***


In our data sets, the query size is never higher than 1000 individuals. We can focus on conditions that are likely to happen considering our data as depicted in figure3. The chi2 p-value increases faster with the query size. However we can notice that when the number of common element is low (2/10) and considering the FDR application, the chi2 p-value would propose more GO terms than the other methods. At this scale for higher common elements the hypergeometric is the method that will tend to propose more GO terms with a higher query size.


### which one to choose ?

For a prospect of getting the most complete enrichment: it would be adapted to combine:
- chi2 to get the GOterms where T or Q size are small and the ratio C/T or C/Q is small as well (small T size can mean a punctually used GOterm synonymous to another more often adopted). The chi2 will also be able to capt the 1/2 ratio (very precis and rare annotation).
- and the hypergeometric to obtain the GOterms that have been often used to annotate the genome (we suppose to be placed on an higher node in the Gene Ontology graph).


### room for improvement

It would be relevant to discuss the methods comparison with respect to the specificity of *Ralstonia*'s annotation.
About the python script to compare the methods, to facilitate the plotting and to make the methods comparable, the chi2 implementation has been adapted. We were confronted to negative values in the contingency table (impossible situations), those values were neutralized as 0. This must probably explains the second half (drop of the p-value after a certain Q size) of the bell shape curve that should have not been plotted.
