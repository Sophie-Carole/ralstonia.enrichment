library(argparser, quietly=TRUE)
library(tidyverse)
library(igraph)
library(magrittr)
library(STRINGdb)

### parsing

p <- arg_parser("Choose your taxon between  #267608 and #511145")
p <- add_argument(p, "--taxon", "-t", help="The id of the taxon as given by strinDB", default=511145)

argv <- parse_args(p)

name=argv$name
taxon=argv$taxon #267608 #511145
if (taxon=="267608"){
  name="ralstonia"} else {
    name="ecoli"}


### neo4j access

print("*** neo4j connexion ***")


if (!require('neo4r')) { # client neo4j (pas dispo avec conda)
  install.packages('neo4r')
}
library(neo4r)
neodb = neo4j_api$new(
  url = "http://localhost:7474", 
  user = "neo4j", 
  password = "bioinfo"
)
cypher = function(query, neo4j=neodb, ...) call_neo4j(query=query, con=neo4j, ...)


## graph extraction and communities research 

# Filtration on connected component > 10


g.string = paste0('MATCH p = (:Protein{taxon_id:',taxon,'})-[r:STRINGdb]-(:Protein{taxon_id:',taxon,'}) WHERE r.coexpression>=700 OR r.ppi>=700 OR r.neighborhood>=700 RETURN p')  %>% cypher(type="graph")

g.string$nodes = g.string$nodes %>% unnest_nodes(what="properties")
g.string$relationships = g.string$relationships %>% 
  unnest_relationships %>% 
  select(startNode, endNode, type, everything()) %>% 
  mutate(coexpression = unlist(coexpression), ppi = unlist(ppi), neighborhood = unlist(neighborhood))
g.string = graph_from_data_frame(d=g.string$relationships, directed=F, vertices = g.string$nodes)

CCs.num = which(clusters(g.string)$csize >=10)
CCs.size = clusters(g.string)$csize[CCs.num]
V.ids = V(g.string)[ clusters(g.string)$membership %in% CCs.num ]

g = induced_subgraph(g.string, V.ids)


# combination score calculation and community detection

prior=0.041
no_prior = function(x, prior = 0.041) (ifelse(is.na(x), 0, x) / 1000 - prior) / (1-prior)
s_coexp_nop = no_prior(E(g)$coexpression)
s_ppi_nop = no_prior(E(g)$ppi)
s_neighborhood_nop = no_prior(E(g)$neighborhood)
s_tot_nop = 1 - (1 - s_coexp_nop) * (1 - s_ppi_nop) * (1 - s_neighborhood_nop)
E(g)$combined_score = round(1000 * (s_tot_nop + prior *(1 - s_tot_nop))) 


g.communities = cluster_louvain(g, weights=E(g)$combined_score)

# name replacement by protein_id only 

g.clusters = lapply(groups(g.communities), function(x) V(g)[ name %in% x]$protein_id)
head(g.clusters, 2)

# communities files writing

lines = sapply(g.clusters, function(x) paste(x, collapse = '\t')) %>% simplify2array 
write_lines(lines, paste0("../enrichment/data/",name,"_g.communities.txt"))

communities = readLines(paste0("../enrichment/data/",name,"_g.communities.txt"))

index=1
while (index <= length(communities)) {
  if (index < 10) {exp="0"} else {exp=""}
  write_lines(communities[index], paste0("../enrichment/data/",name,"_g.com.",exp,index,".txt"))
  index=index+1
}


## overrepresented GOterms research 

#'""GOterm collection function
#direct : all = F
#indirect : all = T


get_GeneProducts = function(goterm, all=F, taxon_id) {
  query = paste0("MATCH (p:Protein{taxon_id:",taxon_id,"})-[:GO_annotation]-(:GOTerm {acc: '", goterm, "'}) RETURN DISTINCT p.protein_id ORDER BY p.protein_id")
  if (all) {
    query = paste0("MATCH (p:Protein{taxon_id:",taxon_id,"})-[:GO_annotation|is_a|part_of*]->(:GOTerm {acc: '", goterm,"'}) RETURN DISTINCT p.protein_id ORDER BY p.protein_id")
  }
  res = query %>% cypher 
  if (length(res) > 0) {
    res = res %>% extract2(1) %>% unlist
  }
  else {
    res = c() #empty vector
  }
  res
}

terms = paste0(" MATCH (:Protein {taxon_id:",taxon,"})-[:GO_annotation|is_a|part_of*]->(t:GOTerm) 
RETURN DISTINCT t.acc ") %>% cypher %>% unlist

go.sets.direct = lapply(terms, get_GeneProducts, taxon_id=taxon)
names(go.sets.direct)=terms

go.sets.implicit = lapply(terms, function(x) get_GeneProducts(x, all=T, taxon_id=taxon))
names(go.sets.implicit)=terms

#file writting

if (taxon==511145) {strain="Escherichia coli K-12 MG1655"} else {strain="Ralstonia Solanacearum GMI 1000"}

header = paste0("# format: sets
# version: 1.0
# strain: ",strain, "
# date: ", format(Sys.time(), '%d %B, %Y'), "
# comment: Gene Ontology terms")
sets = lapply(terms, function(x) paste(go.sets.direct[[x]], collapse = '|')) %>% unlist
tb = tibble(GOTerm=terms, GeneProducts = sets)
go.terms = read_tsv("neo4j/download/neo4j-community/import/go.terms.tsv")
lines = tb %>% inner_join(go.terms, by=c('GOTerm'='acc')) %>% mutate(desc=paste0(term_type,': ', name)) %>% mutate(txt=paste(GOTerm, desc, GeneProducts, sep='\t')) %>% filter(str_detect(GeneProducts,"\\|"))
write_lines(header,paste0("../enrichment/data/",name,".go.direct.sets"))
write_lines(lines$txt,paste0("../enrichment/data/",name,".go.direct.sets"), append=T)

header = paste0("# format: sets
# version: 1.0
# strain: ",strain, "
# date: ", format(Sys.time(), '%d %B, %Y'), "
# comment: Gene Ontology terms")
sets = lapply(terms, function(x) paste(go.sets.implicit[[x]], collapse = '|')) %>% unlist
tb = tibble(GOTerm=terms, GeneProducts = sets)
go.terms = read_tsv("neo4j/download/neo4j-community/import/go.terms.tsv")
lines = tb %>% inner_join(go.terms, by=c('GOTerm'='acc')) %>% mutate(desc=paste0(term_type,': ', name)) %>% mutate(txt=paste(GOTerm, desc, GeneProducts, sep='\t')) %>% filter(str_detect(GeneProducts,"\\|"))
write_lines(header,paste0("../enrichment/data/",name,".go.implicit.sets"))
write_lines(lines$txt,paste0("../enrichment/data/",name,".go.implicit.sets"), append=T)

