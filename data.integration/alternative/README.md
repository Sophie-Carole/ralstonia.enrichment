# Ralstonia enrichment - loading_script.R (augmented)

This script loads data from StringDB and GO DB into neo4j

**! TO DO BEFORE RUNNING THE SCRIPT**

To clone Ralstonia.enrichment repository :

```{bash}
git clone https://gitlab.com/Sophie-Carole/ralstonia.enrichment.git

```

### files required

*To put in the string.data directory:*

You will need to download from stringDB website (https://version-11-0.string-db.org/cgi/download.pl?sessionId=g6n8yY32AWLw) the following files:
+ [511145-267608].protein.links.detailed.v11.0.txt.gz 
+ [511145-267608].protein.actions.v11.0.txt.gz
+ COG.mappings.v11.0.txt.gz


*To put in go.data directory*

You need to download from geneontology ; 
```{bash}
wget http://archive.geneontology.org/latest-termdb/go_daily-termdb-data.gz
```

The following gene ontology annotation files: 
```{bash}
curl ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/18.E_coli_MG1655.goa -o 18.E_coli_MG1655.goa

curl ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/72.R_solanacearum.goa -o 72.R_solanacearum.goa
```

### neo4j access

Neo4j must have been downloaded from :
https://neo4j.com/download-center/#community (go to *Community server*)

To do from the data.integration directory

```{bash eval=F}
mkdir -p neo4j/download
cd neo4j/download
```

Put the downloaded tar.gz file in neo4j/download directory

```{bash eval=F}
version=4.1.4
tar xf neo4j-community-$version-unix.tar.gz
ln -s neo4j-community-$version neo4j-community
cd neo4j-community/
```

The server must have been started from the shell 

To start and stop the server (shell):
```{bash eval=F}
./bin/neo4j console
```

In your browser put :

http://localhost:7474/browser/

first authentification:
username: neo4j
password : neo4j

change password: bioinfo 


### Gene Ontology database restauration (mariadb) and access 


```{bash eval=F}
dbname=go_idh_2020
mysql="mysql -uroot -p[password] -h 127.0.0.1 $dbname"
mysql -uroot -p[password] -e "CREATE DATABASE $dbname"
```

in go.data directory:
```{bash eval=F}
gunzip go_daily-termdb-data.gz
$mysql < go_daily-termdb-data
```

### GOterm extraction

tsv file writing containing GOterms and their types 

```{bash eval=F}
$mysql -e "SELECT id, acc, term_type, name FROM term WHERE acc LIKE 'GO:%' AND is_obsolete=0;" > neo4j/download/neo4j-community/import/go.terms.tsv
```

```{bash eval=F}
is_a=1
part_of=20
$mysql -B -e "SELECT term1_id, term2_id FROM term2term WHERE relationship_type_id = $is_a;" > neo4j/download/neo4j-community/import/go.rel.is_a.dump.tsv
$mysql -B -e "SELECT term1_id, term2_id FROM term2term WHERE relationship_type_id = $part_of;" > neo4j/download/neo4j-community/import/go.rel.part_of.dump.tsv
```


### Orthologous links between *R. solanacearum* and *E. coli*

in data.string directory

```{bash eval=F}
less COG.mappings.v11.0.txt.gz | head -n 1 > 511145.COG.mappings.v11.0.txt.gz
less COG.mappings.v11.0.txt.gz | head -n 1 > 267608.COG.mappings.v11.0.txt.gz


less COG.mappings.v11.0.txt.gz | grep 511145 >> 511145.COG.mappings.v11.0.txt.gz
less COG.mappings.v11.0.txt.gz | grep 267608 >> 267608.COG.mappings.v11.0.txt.gz
```


### Running loading_script.R

move loading_script.R into `data.integration` directory

from `data.integration` directory:

```{R}
Rscript loading_script.R -t 267608

Rscript loading_script.R 
```

----

# Ralstonia enrichment - `building_sets_script.R`

### Running `building_sets_script.R`

from `data.integration` directory:

```{R}
Rscript building_sets_script.R -t 267608

Rscript building_sets_script.R
```