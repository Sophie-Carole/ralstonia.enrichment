The R scripts in this section require the following packages:

- tidyverse
- magrittr
- igraph
- STRINGdb
- argparser

---


# Ralstonia enrichment - `loading_script.R`

This script allows to load data from StringDB and GO DB into your neo4j local instance 


**! TO DO BEFORE RUNNING THE SCRIPT**

To clone `Ralstonia.enrichment` repository :

```{bash}
git clone https://gitlab.com/Sophie-Carole/ralstonia.enrichment.git

```


### neo4j access

Neo4j must have been downloaded from :
https://neo4j.com/download-center/#community (go to *Community server*)

To do from the `data.integration` directory

```{bash eval=F}
mkdir -p neo4j/download
cd neo4j/download
```

Put the downloaded tar.gz file in `neo4j/download` directory

```{bash eval=F}
version=4.1.4
tar xf neo4j-community-$version-unix.tar.gz
ln -s neo4j-community-$version neo4j-community
cd neo4j-community/
```

The server must have been started from the shell 

To start and stop the server (shell):
```{bash eval=F}
./bin/neo4j console
```

In your browser put :

http://localhost:7474/browser/

first authentification:
username: neo4j
password : neo4j

change password: bioinfo 


copy the content of `data.integration/import` directory into the neo4j import directory

by doing for instance

```{bash eval=F}
cp ../../../import/* neo4j-community/import/.
```
-----

### Running `loading_script.R`

from `data.integration` directory:


```{R}
Rscript loading_script.R -t 267608

Rscript loading_script.R 
```

----

# Ralstonia enrichment - `building_sets_script.R`

### Running `building_sets_script.R`

from `data.integration` directory:

```{R}
Rscript building_sets_script.R -t 267608

Rscript building_sets_script.R
```