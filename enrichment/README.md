# enrichment
Simple script to perform set enrichment analysis

`blastset.py` allows to perform a gene set enrichment analysis from the command line. It takes as input the gene set and will compare it to all the sets in a file containing predefined sets (usually a set corresponds to genes sharing an annotation, *e.g.* the genes annotated with the [Gene Ontology](http://geneontology.org) term GO:0006418 *tRNA aminoacylation for protein translation*	from the Biological Process branch).

## set comparisons

The **query set** is compared to each **target set** through a **coverage** value that informs about the extend of the overlap between the two sets and a  **statistical test**, thus each comparison yields a *p-value* which can be interpreted as the probability to obtain by chance at least the number of elements in common between the two compared sets (considered as two random samples from the same population).

Statistical test available :
- binomial
- hypergeometric
- chi2

# Example
The following command line will consider the set of identifiers {b0021, b0274, b1893, b0022, b0264, b0988, b1894, b0265, b3444, b3445, b0275, b4516} and search for most *similar* sets in the file `data/ecoli.go.direct.sets`. The *p-values* are adjusted by FDR because of the multiple testing performed and the threshold for *p-values* is set to 0.05.


```sh
./blastset.py --query 'b0021   b0274   b1893   b0022   b0264   b0988   b1894   b0265   b3444   b3445   b0275   b4516' --sets data/ecoli.go.direct.sets --adjust --alpha 0.05
```

Or

```sh
python blastset.py -q data/ecoli_g.com.01.txt -t data/ecoli.go.direct.sets --adjust --alpha 0.05 --method binomial
```


The output produced is:
<pre>
#GO_id	coverage	pvalue	common	GO_name	common_elements
GO:0006313	0.75	1.478122504104189e-23	12/48	biological_process: transposition, DNA-mediated	b0022, b0265, b0275, b1893, b0274, b1894, b4516, b0988, b0021, b3444, b3445, b0264
GO:0032196	0.7894736842105263	1.1622882533903894e-22	12/57	biological_process: transposition	b0022, b0265, b0275, b1893, b0274, b1894, b4516, b0988, b0021, b3444, b3445, b0264
GO:0006310	0.8878504672897196	2.225465739447219e-19	12/107	biological_process: DNA recombination	b0022, b0265, b0275, b1893, b0274, b1894, b4516, b0988, b0021, b3444, b3445, b0264
GO:0004803	0.9230769230769231	9.697608347281728e-10	6/39	molecular_function: transposase activity	b3445, b1893, b0274, b0988, b0021, b0264
</pre>
