#!/usr/bin/python
#Script coded in python 3 


import argparse
import numpy as np
from os.path import isfile
from scipy.stats import binom, hypergeom, chi2_contingency
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser(description='methods comparison')
parser.add_argument('-m', '--measure', required=False, default='binomial', help='binomial (default), hypergeometric or chi2 . ')
parser.add_argument('-f', '--focus', required=False, default='no', help='yes for the plot focused on Q[0,1000] and pval[0,0.2]')


param = parser.parse_args()


def coverage(query_size, common_elements, elements):
    pval = 1 - (((common_elements) / query_size) * ((common_elements) / float(elements)))
    return pval

def binomial(query_size, common_elements, elements, population_size):
    pval = binom.cdf( query_size - (common_elements), query_size, 1 - float((elements)) / population_size)
    return pval

def hypergeometric(query_size, common_elements, elements, population_size):
    pval = hypergeom.sf((common_elements) - 1, population_size, float(elements), query_size)
    return pval

def chi2(query_size, common_elements, elements, population_size):
    observation = int(population_size - query_size - elements + common_elements)
    #print(population_size - (elements))
    #print(query_size - (common_elements))
    #print((elements) - (common_elements))
    #print(population_size - query_size)

    qs_ce=query_size - (common_elements)
    if qs_ce < 0:
        qs_ce = 0

    if observation <= 0:
        observation = 0

    contingency_table = np.array([[(common_elements), qs_ce,query_size], [(elements) - (common_elements), observation, population_size - query_size], [(elements), population_size - (elements), population_size]])

    chi2, pval, dof, ex = chi2_contingency(contingency_table)
    return pval



def pval_measured (Q, C, T, G, method):
    if method == 'binomial': 
        pval = binomial(Q, C, T, G)
    elif method == 'hypergeometric':
        pval = hypergeometric(Q, C, T, G)
    elif method== 'chi2' :
        pval = chi2(Q,C,T,G)
    elif method== 'coverage' :
        pval = coverage(Q,C,T)
    return pval


def test_method (G, T, method, step):
    C_tab=[]
    pval_tab=[]
    i=0
    for C in range (0, T, step):
        C_tab.append(C)
        Q_tab=[]
        for Q in range (10, (G), 10):
            
            i+=1
            Q_tab.append(Q)
            pval = pval_measured (Q, C, T, G, method)
            pval =abs(pval)
            pval_tab.append(pval)


    return Q_tab, pval_tab, C_tab, G, T




def plot (Q_tab, pval_tab, C_tab, G, T, focus):

    Q=np.array(Q_tab)
    pval=np.array(pval_tab)

    i=0

    for el in range(0, len(pval), len(Q)):
       
        deb=el
        fin=deb+len(Q)
        lab=C_tab[i]
        i=i+1
        print(np.array(pval_tab[deb:fin]))

        plt.plot(Q,np.array(pval_tab[deb:fin]), label=lab)
        
        if focus =="yes":
            plt.xlim(0,1000)
            plt.ylim(0, 0.2)

    plt.suptitle(param.measure+' - with population size = '+str(G)+' and target size = '+str(T), fontsize=12)
    plt.ylabel("p-value")
    plt.xlabel("query size")
    plt.legend(loc='upper right', title='n of common \n elements')
    plt.show()



# test when G ~ T both large (5000)

Q, pval, C, G, T= test_method(5000, 4900, param.measure, 500)
plot(Q, pval, C, G, T, param.focus)

# test when G ~ T both small (500)

Q, pval, C, G, T=test_method(500, 490, param.measure, 100)
plot(Q, pval, C, G, T, param.focus)

# test when G = 5000 (average proteome size) and T = 500 (corresponds +- to the largest target in our direct dataset)

Q, pval, C, G, T=test_method(5000, 500, param.measure, 100)
plot(Q, pval, C, G, T, param.focus)

# test when G = 5000 and T = 50

Q, pval, C, G, T=test_method(5000, 50, param.measure, 10)
plot(Q, pval, C, G, T, param.focus)

# test when G = 5000 and T = 10

Q, pval, C, G, T=test_method(5000, 10, param.measure, 2)
plot(Q, pval, C, G, T, param.focus)

# test when G = 5000 and T = 5  (G very large and T small)

Q, pval, C, G, T=test_method(5000, 5, param.measure, 1)
plot(Q, pval, C, G, T, param.focus)
